$(document).ready(function() {


    add_to_budget();

    $('#explore ul li').click(function() {
        var item = this;
        $('#explore').addClass("list-active");
        $('#explore').addClass(item.className);
    })

    $('#explore #card-item-list .item-card .item-card-image').click(function() {
        var card = $(this).parent();
        var card_clone = $(card).clone();
        $('#single-item-view').addClass("active");
        $('body').addClass("modal-active");
        $('#single-item-view').append(card_clone);
        add_to_budget();
        additional_info();
    })

    $('.close-single-view').click(function() {
        $('#single-item-view').removeClass("active");
        $('body').removeClass("modal-active");
        $('#single-item-view').empty();
    })


});

function additional_info() {
    $('#single-item-view .item-card .product-actions a.more-details').click(function() {
        var info = this;
        var parent = $(info).parent().parent().parent();
        parent.toggleClass("info-active");
    })
}

function add_to_budget() {
    $('.add-to-budget').click(function() {
        var cart_item = $(this).find("span");
        remove_item();
        jQuery(this).addClass("added-to-budget");
        $(cart_item).html("Tilføjet!");
    })
}

function remove_item() {
    $('.added-to-budget').click(function() {

        var cart_item = $(this).find("span");
        if ($(this).hasClass("added-to-budget")) {
            Swal.fire({
                title: 'Sikker?',
                text: "Er du sikker på du vil fjerne dette fra dit budget?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ja, jeg er sikker'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Fjernet!',
                    )
                    $(this).removeClass("added-to-budget")
                    $(cart_item).html("Tilføj til budget");
                }
            })
        }
    })
}