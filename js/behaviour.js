$(document).ready(function() {
    // På ready, kalder vi funktionen reminder, det gør funktionen dynamisk at anvende i andre scenarier end loading af siden
    reminders();



    // Countdown funktionen som tæller ned fra denne dato
    $("#time-date").countdown("2019/06/01", function(event) {
        $(this).text(
            event.strftime('%D dage %H timer %M minutter %S sekunder')
        );
    });

    $("#create-reminder a").click(function() {
        $("#overlays").addClass("active");
        $("body").addClass("modal-active");
    });



    $("#close-reminder-overlay").click(function() {
        $("#overlays").removeClass("active");
        $("body").removeClass("modal-active");
    });

    $("form").submit(function(e) {
        // Her fjerner vi default funktionen fra formularer hvordan de sender dataer, da vi gerne vil have en dynamisk indsættelse
        e.preventDefault();
    });
    $("form button").click(function() {
        // Få fat i informationerne fra form input
        var title = jQuery('#title-input').val();
        var date = jQuery('#date-input').val();
        // Her splitter vi datoen op i variabler
        var date_day = date.slice(-2); // => "2 last characters"
        var date_month = date.slice(5, 7);
        // Her konverterer vi talene fra slice til måned string, så der ikke bare står 02, men står februar i stedet
        if (date_month == 1) {
            month = "Januar"
        } else if (date_month == 2) {
            month = "Februar"
        } else if (date_month == 3) {
            month = "Marts"
        } else if (date_month == 4) {
            month = "April"
        } else if (date_month == 5) {
            month = "Maj"
        } else if (date_month == 6) {
            month = "Juni"
        } else if (date_month == 7) {
            month = "Juli"
        } else if (date_month == 8) {
            month = "August"
        } else if (date_month == 9) {
            month = "September"
        } else if (date_month == 10) {
            month = "Oktober"
        } else if (date_month == 11) {
            month = "November"
        } else if (date_month == 12) {
            month = "December"
        } else {
            month = "Error"
        }
        var template = "<section class='event-reminder'><article><svg class='checkmark' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 52 52'><circle class='checkmark__circle' cx='26' cy='26' r='25' fill='none'></circle><path class='checkmark__check' fill='none' d='M14.1 27.2l7.1 7.2 16.7-16.8'></path></svg><h2>" + title + "</h2><time datetime='" + date + "'class='event-date'>" + date_day + "<span class='month'>" + month + "</span></time></article>";
        $('#reminders').prepend(template);
        // Her kalder vi funktionen igen, så vi også kan complete disse nye tasks vi lige har oprettet
        reminders();

        $("#overlays").removeClass("active");
        $("body").removeClass("modal-active");

    })
});

function reminders() {

    $('section.event-reminder').click(function() {
        // Denne funktion laver en UX hvor folk ikke ved en fejl kommer til at fjerne en af deres allerede completed tasks
        if ($(this).hasClass("complete")) {
            Swal.fire({
                title: 'Sikker?',
                text: "Er du sikker på du vil sætte den som aktiv igen?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ja, jeg er sikker'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Success',
                    )
                    $(this).removeClass("complete")
                }
            })
        } else {
            // Hvis ikke den allerede er completed, så completer vi den
            $(this).addClass("complete");
        }
    });
}